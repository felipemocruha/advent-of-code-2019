(ns day1.core
  (:require [clojure.java.io :as io])
  (:gen-class))


(defn required-fuel [mass]
  (-> mass (/ 3) Math/floor (- 2) int))

(defn calculate-total [all]
  (reduce #(+ %1 (required-fuel (read-string %2))) 0 all))

(defn -main [& args]
  (-> *in* io/reader line-seq calculate-total println))
    
