(ns day1.core-test
  (:require [day1.core :as core]
            [midje.sweet :refer :all]))


(fact "to find the fuel required for a module, take its mass, divide by three, round down, and subtract 2"
  (core/required-fuel 12) => 2
  (core/required-fuel 14) => 2
  (core/required-fuel 1969) => 654
  (core/required-fuel 100756) => 33583)

(fact "the total is the sum of all individual required amounts of fuel"
  (core/calculate-total '("12" "14" "1969" "100756")) => 34241)
